<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 19.01.18
 * Time: 15:49
 */

namespace Johnny;

trait HasManyThroughManyRelation
{
    public function hasManyThroughMany($related, $through, $localKey, $foreignKey, $throughForeignKey, $pivotKey, $pivotThroughKey, $pivotTable)
    {
        $model = new $related;
        $table = $model->getTable();
        $throughModel = new $through;
        $pivot = $throughModel->getTable();

        return $model
            ->join($pivotTable, $pivotTable . '.' . $pivotKey, '=', $table . '.' . $foreignKey)
            ->join($pivot, $pivot . '.' . $throughForeignKey, '=', $pivotTable . '.' . $pivotThroughKey)
            ->select($table . '.*')
            ->where($pivot . '.' . $localKey, '=', $this->id);
    }
}